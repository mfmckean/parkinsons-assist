//
//  FirstViewController.swift
//  ParkinsonsAssist
//
//  Created by Cooper Birks on 10/25/19.
//  Copyright © 2019 T3am SFU. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    @IBOutlet weak var messaging: UIButton!
    @IBOutlet weak var health: UIButton!
    @IBOutlet weak var settings: UIButton!
    
    override func viewDidLoad(){
        messaging.layer.cornerRadius = messaging.frame.height * 0.5
        health.layer.cornerRadius = health.frame.height * 0.5
        settings.layer.cornerRadius = settings.frame.height * 0.5
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    

}

