//
//  SecondViewController.swift
//  ParkinsonsAssist
//
//  Created by Cooper Birks on 10/25/19.
//  Copyright © 2019 T3am SFU. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    @IBOutlet weak var back: UIButton!
    @IBOutlet weak var pageUp: UIButton!
    @IBOutlet weak var pageDown: UIButton!
    
    override func viewDidLoad() {
        back.layer.cornerRadius = back.frame.height * 0.5
        pageUp.layer.cornerRadius = pageUp.frame.height * 0.5
        pageDown.layer.cornerRadius = pageDown.frame.height * 0.5
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
}

